# HallwaySign-Digispark-ATTiny85

A RGB led-driven hallway sign.

### Components

* Digispark USB Development board (clone) Type A
** https://www.aliexpress.com/item/32279051124.html?spm=a2g0s.9042311.0.0.27424c4ddibatC

* RF (microwave radar) sensor
** https://www.aliexpress.com/item/32787712136.html?spm=a2g0s.9042311.0.0.27424c4dnf25jC

* Acrylic engraved sign
* RGB LED strip
* 3D printed mounting plate

### General Description

A sign to mount on the wall outside of my office. Mostly just to have some lights in the hallway. These will turn off when no one's around, and fire up when the RF sensor detects motion nearby.
