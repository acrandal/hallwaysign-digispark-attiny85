/**
 *  Digispark based lightup colorful LED sign
 * 
 *  @author Aaron S. Crandall <acrandal@gmail.com>
 *  @copyright 2020
 *  @license Standard copyright
 * 
 *  @version v1.0
 */


#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket and ATTiny85
#endif

#define RF_DETECT_PIN   0       // RF deflection sensor input pin
#define BUILTIN_LED_PIN 1       // Digispark model A builtin LED on P1
#define RGB_LED_PIN     2       // Digispark pin P2

#define LED_COUNT       18      // LEDs on RGB LED strip
#define LED_BRIGHTNESS  30      // One time brightness in 0..255

Adafruit_NeoPixel strip(LED_COUNT, RGB_LED_PIN, NEO_GRB + NEO_KHZ800);
boolean needs_wipe = false;

// Color pallet in use
uint32_t guBlue   = strip.Color(8, 60, 150);
uint32_t guRed    = strip.Color(200,16,46);
uint32_t guWhite  = strip.Color(110,110,110);
uint32_t guGray   = strip.Color(193,198,200);
uint32_t off      = strip.Color(0,0,0);
uint32_t alert_white = strip.Color( 150, 150, 150 );

enum light_show { GO_GU, RAINBOW };
light_show selected_show = GO_GU;

/**
 * One run initialization routine - hardware setup
 */
void setup() {
  randomSeed(analogRead(4));        // Read relatively random number from analog pin
  
  pinMode(BUILTIN_LED_PIN, OUTPUT); // LED on Model A 
  pinMode(RF_DETECT_PIN, INPUT);    // RF radio sensor

  // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
  // Any other board, you can remove this part (but no harm leaving it):
  // This is required for ATTiny 85 boards, such as the Digispark
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.


  strip.begin();                        // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();                         // Turn OFF all pixels ASAP
  strip.setBrightness(LED_BRIGHTNESS);  // Set BRIGHTNESS to about 1/5 (max = 255)
  
}


/**
 * Repeated run loop function 
 */
void loop() {
  if(digitalRead(RF_DETECT_PIN) == HIGH) {
    digitalWrite(BUILTIN_LED_PIN, HIGH);

    if( needs_wipe == false ) {         // First alert step
      wakeup_wipe(75);
      needs_wipe = true;

      // Select which show to do until detect goes low
      if( random(100) < 10 ) {
        selected_show = RAINBOW;
      } else {
        selected_show = GO_GU;
      }
    }

    if( selected_show == RAINBOW ) {
      rainbow(20);
    } else if( selected_show == GO_GU ) {
      goGU(200, 72);
    }

    //wakeup_wipe(75);
    //goGU(150, 36);
    needs_wipe = true;
  } else {
    digitalWrite(BUILTIN_LED_PIN, LOW);

    if( needs_wipe ) {
      wakeup_wipe(30);
      needs_wipe = false;
    }
  }
}

/**
 *  Wakeup attention getter
 */
void wakeup_wipe(int wait) {
  int downer = LED_COUNT - 1;

  for( int upper = 0; upper < LED_COUNT; upper++, downer-- ) {
    if( upper > 0 ) {
      strip.setPixelColor(upper - 1, off);
    }
    if( downer < LED_COUNT - 1 ) {
      strip.setPixelColor(downer + 1, off);
    }
    strip.setPixelColor(upper, guBlue);
    strip.setPixelColor(downer, guWhite);
    strip.show();

    delay(wait);
  }
  strip.setPixelColor(0, off);
  strip.setPixelColor(LED_COUNT - 1, off);
  strip.show();
}



/**
 *  Show a Go GU! Color wheel/scheme
 */
void goGU(int wait, int ticks) {
  #define COLORS_COUNT 9
  uint32_t colors[COLORS_COUNT] = {guBlue, guBlue, guBlue, guWhite, guBlue, guBlue, guBlue, guWhite, guRed};

  for( int tick = 0; tick < ticks; tick++ ) {
    for( int index = 0; index < LED_COUNT; index++ ) {
      int pixel_index = (tick + index) % LED_COUNT;
      int color_index = index % COLORS_COUNT;
      strip.setPixelColor(pixel_index, colors[color_index]);
    }
    strip.show();
    delay(wait);
  }
}


void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}

// Rainbow with default 2 passes
void rainbow(int wait) {
  rainbow(wait, 2);
}

// Rainbow cycle along whole strip. Pass delay time (in ms) between frames.
void rainbow(int wait, int passes) {
  // Hue of first pixel runs 5 complete loops through the color wheel.
  // Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to 5*65536. Adding 256 to firstPixelHue each time
  // means we'll make 5*65536/256 = 1280 passes through this outer loop:
  for(long firstPixelHue = 0; firstPixelHue < passes * 65536; firstPixelHue += 256) {
    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the single-argument hue variant. The result
      // is passed through strip.gamma32() to provide 'truer' colors
      // before assigning to each pixel:
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show(); // Update strip with new contents
    delay(wait);  // Pause for a moment
  }
}
